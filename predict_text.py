
from helpers import get_letter

# import mnist_loader
# training_data, validation_data, test_data = mnist_loader.load_data_wrapper()

from training_nn import network2_
import numpy as np
from nn_two_stage.second_nn import get_let_from_2nd_nn_ijltIL1
from nn_two_stage.second_nn import get_let_from_2nd_nn_ceg


def get_string_from_nn(all_letters):
	net = network2_.Network([1024, 30, 66], cost=network2_.CrossEntropyCost)
	
	biases_saved = np.load('model_output/biases.npy', allow_pickle=True, encoding ='latin1') # +1 for the outgoing
	weights_saved = np.load('model_output/weights.npy', allow_pickle=True, encoding ='latin1') # strength of the connection between layers

	word_string = ""
	
	i = 0
	for x in all_letters:
		output = np.argmax(net.feedforward(x, biases_saved = biases_saved, weights_saved = weights_saved))
		
		#second stage classification below
		if output in (18, 19, 21, 29, 44, 47, 1): # i j l t I L 1
			output = get_let_from_2nd_nn_ijltIL1(x)
		elif output in (12, 14, 42): # c e G
			output = get_let_from_2nd_nn_ceg(x)
			
		word_string = word_string + get_letter(output)
		i = i + 1

	return word_string

	#print np.argmax(net.feedforward(test_data[502][0], biases_saved = biases_saved, weights_saved = weights_saved))