import cv2
import numpy as np

def image_for_detection(raw_image):

	#remove tiny noises by blurring
	sm_image = cv2.GaussianBlur(raw_image,(5,5),0)
	
	#binarize
	ret, bw_image = cv2.threshold(sm_image,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
	
	#dilate   helps the horizontal projection
	kernel = np.ones((2,2),np.uint8)
	bw_image = cv2.dilate(bw_image,kernel)
	
	return bw_image
	
def image_for_extraction(raw_image):

	# remove tiny noises by blurring
	raw_image = cv2.GaussianBlur(raw_image,(3,3),0)

	# binarize
	ret,no_sm_bw_image = cv2.threshold(raw_image,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
	
	return no_sm_bw_image


def tilt_image_skew(thresh):
	coords = np.column_stack(np.where(thresh > 0))
	angle = cv2.minAreaRect(coords)[-1]
	if angle < -45:
		angle = -(90 + angle)
	else:
		angle = -angle

	# rotate the image to deskew it
	(h, w) = thresh.shape[:2]
	center = (w // 2, h // 2)
	M = cv2.getRotationMatrix2D(center, angle, 1.0)
	rotated = cv2.warpAffine(thresh, M, (w, h),
		flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)

	return rotated
