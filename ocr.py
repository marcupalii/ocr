
import cv2
import os
from segmentation import get_characters, get_words
from predict_text import get_string_from_nn
from dictionary_correction import correction


def perform_ocr(img_url):
	

	use_dict = True
	
	raw_image = cv2.imread(img_url, 0)

	# get all the words (as an numpy image array), words on each line, and maximum height on that line
	all_words, words_on_line, max_height_on_line = get_words(raw_image)

	print('words per line: {0}\ntotal lines: {1}\ntotal words: {2}\n\n'.format(words_on_line,len(words_on_line),len(all_words)))

	# to write the output into a file
	fp = open("Steps\output.txt", 'w')
	fp.truncate()

	count = 0
	for i in range(0, len(words_on_line)):

		for j in range(0, words_on_line[i]):
			
			all_characters = get_characters(all_words[count],max_height_on_line[i],i,j)
			
			if use_dict:
				print (correction(get_string_from_nn(all_characters)),
				fp.write(correction(get_string_from_nn(all_characters))))
				fp.write(" ")
			else:
				print (get_string_from_nn(all_characters),
				fp.write(get_string_from_nn(all_characters)))
				fp.write(" ")

			count = count + 1
			
		print("\n")
		fp.write("\n")

	fp.close()


if __name__ == "__main__":
	perform_ocr('D:\REPO\Gitlab\ocr\image_samples\DSC_0511.JPG')
	os.startfile("Steps\output.txt")
