
import numpy as np
import cv2
from preprocessing import image_for_detection,tilt_image_skew,image_for_extraction

from functions_lines import findLines
from functions_lines import get_lines_threshold

from functions_words import findSpaces
from functions_words import get_spaces_threshold
from functions_characters import fix_i_j
from helpers import div_


def get_characters(raw_image,max_line_height,line,word):

	# === Find Contours

	mo_image = raw_image.copy()
	contour0 = cv2.findContours(mo_image.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
	contours = [cv2.approxPolyDP(cnt,2,True) for cnt in contour0[0]]


	# === Extract Bounding Rectangles
	maxArea = 0
	rect=[]
	for ctr in contours:
		maxArea = max(maxArea,cv2.contourArea(ctr))

	areaRatio = 0.008

	for ctr in contours:
		if cv2.contourArea(ctr) > maxArea * areaRatio: 
			rect.append(cv2.boundingRect(cv2.approxPolyDP(ctr,1,True)))
			
	#Find max_line_height and width
	max_w = 0
	
	for i in rect:
		x = i[0]
		y = i[1]
		w = i[2]
		h = i[3]

		if(w>max_w):
			max_w = w

	# Sort rect left to right, top to bottom, plus correct the dots and commas

	#sort all rect by their x
	rect.sort(key=lambda b: b[0])

	#There are two contours detected for the characters such as i and j. So we need to merge two contours of 'dot' and the 'base' of i and j
	#Fix i and j
	rect = fix_i_j(rect, max_line_height, max_w)
	
	# remove artifacts - usually artifacts found are manipulated as 0 height by the i&j dot fixing functions
	minus_count = 0
	minus_list = []
	for i in rect:
		x = i[0]
		y = i[1]
		w = i[2]
		h = i[3]
		
		if h<0:
			minus_list.append(minus_count)
		
		minus_count = minus_count + 1
	
	rect = np.delete(rect, minus_list, axis=0)
	# ================= end ===============================
	
	rect_segmented_image = mo_image.copy()
	
	symbols=[]

	all_letters = []

	#count used for filename naming
	count = 0

	#raw_input('>')
	for i in rect:
		x = i[0]
		y = i[1]
		w = i[2]
		h = i[3]
		
		p1 = (x,y)
		p2 = (x+w,y+h)
		
		letter = mo_image[y:y+h,x:x+w]
		
		#resize letter image to 32x32 ======================================
		#resize letter content to 28x28
		
		o_height = letter.shape[0]
		o_width = letter.shape[1]
		
		#if errors occurs due to the unwanted artifacts, then the height will somehow become zero.
		if (o_height == 0):
			letter = np.zeros((30, 30, 1), np.uint8)
			o_height = letter.shape[0]
			o_width = letter.shape[1]
		
		#resize height to 28 pixels
		#we need three different conditions to work well with the aspect ratios
		
		if(o_height>o_width): # height greater than width

			aspectRatio = div_(o_width,(o_height*1.0))
			
			height = 26
			width = int(height * aspectRatio)
			letter = cv2.resize(letter, (width,height))
			
			#add border which results adding of padding
			
			remaining_pixels_w = abs(32 - letter.shape[1])
			add_left = div_(remaining_pixels_w , 2)
			add_right = remaining_pixels_w - add_left
			letter = cv2.copyMakeBorder(letter, 0, 0, add_left, add_right, cv2.BORDER_CONSTANT, value=(0,0,0))
			
			remaining_pixels_h = abs(32 - letter.shape[0])
			add_top = div_(remaining_pixels_h , 2)
			add_bottom = remaining_pixels_h - add_top
			letter = cv2.copyMakeBorder(letter, add_top, add_bottom, 0, 0, cv2.BORDER_CONSTANT, value=(0,0,0))
			
			# =================
			
		elif(o_width>o_height): # width greater than height
			
			aspectRatio = div_(o_height , (o_width*1.0))
			
			width = 26
			height = int(width * aspectRatio)
			
			letter = cv2.resize(letter, (width,height))
			
			#add border which results adding of padding
			remaining_pixels_w = abs(32 - letter.shape[1])
			add_left = div_(remaining_pixels_w , 2)
			add_right = remaining_pixels_w - add_left
			letter = cv2.copyMakeBorder(letter, 0, 0, add_left, add_right, cv2.BORDER_CONSTANT, value=(0,0,0))
			
			remaining_pixels_h = abs(32 - letter.shape[0])
			add_top =div_(remaining_pixels_h , 2)
			add_bottom = remaining_pixels_h - add_top
			letter = cv2.copyMakeBorder(letter, add_top, add_bottom, 0, 0, cv2.BORDER_CONSTANT, value=(0,0,0))
			
			# =================
		
		else: # both height and width equal
			letter = cv2.resize(letter, (26,26))
			
			#add border which results adding of padding
			remaining_pixels_w = abs(32 - letter.shape[1])
			add_left =div_( remaining_pixels_w , 2)
			add_right = remaining_pixels_w - add_left
			letter = cv2.copyMakeBorder(letter, 0, 0, add_left, add_right, cv2.BORDER_CONSTANT, value=(0,0,0))
			
			remaining_pixels_h = abs(32 - letter.shape[0])
			add_top = div_(remaining_pixels_h, 2)
			add_bottom = remaining_pixels_h - add_top
			letter = cv2.copyMakeBorder(letter, add_top, add_bottom, 0, 0, cv2.BORDER_CONSTANT, value=(0,0,0))
			
			# =================

		count = count + 1
		letter = letter / 255.0

		letter = np.reshape(letter,(1024,1))
		
		all_letters.append(letter)

		#=================================

		cv2.rectangle(rect_segmented_image,p1,p2,255,1)

	cv2.imwrite('Steps\segmented_6.png', rect_segmented_image)

	return all_letters




def get_words(raw_image):
	# Returns a list/array of all the words found along with the number of words on each line.
	cv2.imwrite('Steps\\raw_image_0.png', raw_image)
	# preprocessing of the image

	# img_for_det used for detecting the character and lines boundaries
	img_for_det = image_for_detection(raw_image)
	cv2.imwrite('Steps\img_for_det_1.png', img_for_det)

	# img_for_ext used for the actual extraction of the characters
	img_for_ext = image_for_extraction(raw_image)
	cv2.imwrite('Steps\img_for_ext_2.png', img_for_ext)

	img_for_ext = tilt_image_skew(img_for_ext)
	img_for_det = tilt_image_skew(img_for_det)

	cv2.imwrite('Steps\img_for_det_tilt_3.png', img_for_det)
	cv2.imwrite('Steps\img_for_ext_tilt_4.png', img_for_ext)

	# get threshold to determine how much gap should be considered as the line gap
	LinesThres = get_lines_threshold(40, img_for_det)
	ycoords = findLines(img_for_det, LinesThres)

	# save image with lines printed ==========
	img_with_lines = img_for_ext.copy()
	for i in ycoords:
		cv2.line(img_with_lines, (0, i), (img_with_lines.shape[1], i), 255, 1)
	cv2.imwrite('Steps\img_with_lines_5.png', img_with_lines)
	# ==========

	### =========== lines detection finish - ===========================

	# calculate max_line_height on each line
	max_height_on_line = []
	for i in range(0, len(ycoords) - 1):  # iterate line

		line = img_for_ext[range(ycoords[i], ycoords[i + 1])]

		# to find max_line_height of each line we find contours again in this line only
		contour0 = cv2.findContours(line.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
		contours = [cv2.approxPolyDP(cnt, 2, True) for cnt in contour0[0]]

		# === Extract Bounding Rectangles
		maxArea = 0
		rect = []
		for ctr in contours:
			maxArea = max(maxArea, cv2.contourArea(ctr))

		areaRatio = 0.008

		for ctr in contours:
			if cv2.contourArea(ctr) > maxArea * areaRatio:
				rect.append(cv2.boundingRect(cv2.approxPolyDP(ctr, 1, True)))

		# Find max_line_height and width
		max_line_height = 0

		for i in rect:
			x = i[0]
			y = i[1]
			w = i[2]
			h = i[3]

			if (h > max_line_height):
				max_line_height = h

		max_height_on_line.append(max_line_height)

	### =========== space in a line detection begins ===================

	# get the threshold to determine how much gap should be considered as the space between the words
	threshold_space = get_spaces_threshold(ycoords, img_for_det)

	# split lines based on the ycoords of the detected lines
	# each line is put into the var 'line' and the words are found
	# based on the threshold_space value.

	words_on_line = []
	all_words = []
	count = 0
	number_of_words = 0

	for i in range(0, len(ycoords) - 1):  # iterate line

		line = img_for_det[range(ycoords[i], ycoords[i + 1])]
		# cv2.imwrite('img/'+str(i)+'.png', line)

		# finding the x-coordinates of the spaces
		xcoords = findSpaces(line, threshold_space)

		# print word delim per line
		for x in xcoords:
			cv2.line(line, (x, 0), (x, line.shape[0]), 255, 1)
		cv2.imwrite('Steps\img\\'+str(i)+'.png', line)

		count = 0

		for j in range(0, len(xcoords) - 1):  # iterate words

			# use image with no smoothing
			line = img_for_ext[range(ycoords[i], ycoords[i + 1])]

			word = line[:, xcoords[j]: xcoords[j + 1]]
			all_words.append(word)
			cv2.imwrite('Steps\img\words\\'+str(number_of_words)+'.png', word)

			count = count + 1
			number_of_words = number_of_words + 1
		# Generate space here

		words_on_line.append(count)
	# Line Change

	return all_words, words_on_line, max_height_on_line